const {Profession, validate} = require('../models/profession');
const {AttentionType} = require('../models/attentionType');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();


router.get('/', async (req,res) =>{
  const profession = await Profession.find().sort('name');
  res.send(profession);
});

router.post('/', async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);


    for(x in req.body.attentionType){
      const typeId = req.body.attentionType[x];
      if(typeId != null){
        const attentionType = await AttentionType.findById(typeId)
        if (!attentionType) return res.status(400).send(error.details[0].message);
      }
    }

    const profession = new Profession({
      name: req.body.name
    });

    for(x in req.body.attentionType){
      const typeId = req.body.attentionType[x];
      if(typeId != null){
        profession.attentionTypes.push(typeId);
      }
    }

    await profession.save();

    res.send(profession);
});

router.get('/:id', async (req,res) =>{
  const profession = await Profession.findById(req.params.id);
  if (!profession) return res.status(400).send('Invalid id for profession.');

  res.send(profession);
});

module.exports = router;