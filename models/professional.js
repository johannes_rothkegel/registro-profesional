const config = require('config');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
var requestify = require('requestify'); 

const professionalSchema =  new mongoose.Schema({
    rut: { type: String, minlength: 6, maxlength: 50, require: true},
    name: { type: String, minlength: 3, maxlength: 50, require: true},
    lastName: { type: String, minlength: 3, maxlength: 150, require: true},
    email: { type: String, minlength: 5, maxlength: 255, require: true},
    phone: { type: String, minlength: 6, maxlength: 50, require: true},
    address: { type: String, minlength: 6, maxlength: 255, require: true},
    password: { type: String, minlength: 6, maxlength: 1024, require: true},
    dateOfBirth: Date,
    isAvailable: {type: Boolean, default: false},
    isValidated: {type: Boolean, default: false},
    evaluation: [ {
        rating: { type: Number, minlength: 0, maxlength: 5},
        comments: { type: String, minlength: 6, maxlength: 250},
        date: { type: Date, default: Date.now},
    }],
    personalPhoto:{
        url: String
    },
    professionalTitle:{
        profession:new mongoose.Schema({
            name: { type: String, minlength: 3, maxlength: 150, require: true}
        }),
        university: { type: String, minlength: 3, maxlength: 150, require: true},
        seniorYear: { type: Date},
    },
    attentionsAvailable: [ {
        _idType: { type: String, minlength: 3, maxlength: 150, require: true}
    } ]
});

professionalSchema.methods.generateAuthToken = function() {
    const token = jwt.sign({_id: this._id, email: this.email}, config.get('jwtPrivateKey'));
    return token;
}

const Professional = mongoose.model('Professional', professionalSchema);

function validateProfessional(professional) {
    const schema = {
        rut: Joi.string().regex(/^\d{8}-\d{1}$/).required(),
        name: Joi.string().min(5).max(50).required(),
        lastName: Joi.string().min(3).max(150).required(),
        email: Joi.string().min(3).max(255).required().email(),
        phone: Joi.string().regex(/^\d{3}-\d{3}-\d{5}$/).required(),
        address: Joi.string().min(5).max(255).required(),
        password: Joi.string().min(3).max(1024).required(),
        dateOfBirth: Joi.date().max('1-1-2000').iso().required(),
        personalPhoto: Joi.object({
            url: Joi.string()
        }),
        professionalTitle: Joi.object({
            idProfession: Joi.objectId().required(),
            university: Joi.string().min(5).max(255).required(),
            seniorYear: Joi.date().max('1-1-2018').iso().required(),
        }),
        attentionsAvailable: Joi.array().items(Joi.object({
            typeId: Joi.objectId()
        }))
    };
    return Joi.validate(professional, schema);
}

function validateEdit(professional) {
    const schema = {
        email: Joi.string().min(3).max(255).required().email(),
        phone: Joi.string().regex(/^\d{3}-\d{3}-\d{5}$/).required(),
        address: Joi.string().min(5).max(255).required(),
        password: Joi.string().min(3).max(1024).required(),
        attentionsAvailable: Joi.array().items(Joi.object({
            typeId: Joi.objectId()
        }))
    };
    return Joi.validate(professional, schema);
}



async function validateTitle(professional){
    const rutp = professional.rut.slice(0,professional.rut.indexOf("-"));
    const url = 'http://webhosting.superdesalud.gob.cl/bases/prestadoresindividuales.nsf/(searchAll2)/Search?SearchView&Query=(FIELD%20rut_pres='+rutp+')&Start=1&count=10';
    var body, url2;
    var p = await requestify.get(url)
    .then(function(response) {
        // Get the response body (JSON parsed - JSON response or jQuery object in case of XML response)
        // Get the response raw body
        
        body = response.getBody();
        url2 = 'http://webhosting.superdesalud.gob.cl/' + body.slice(body.indexOf("/bases",1118), body.indexOf("OpenDocument")+12);
        
        body = body.slice(body.indexOf("OpenDocument")+15, body.indexOf("</table>")).split("</td><td>");
        body[1] = body[0].split(",");

        
     //   await professional.save();
    });



    var rut;
    var p = await requestify.get(url2)
    .then(function(response) {
        const s = response.getBody();
        const index = s.indexOf("Rut")+11;
        rut = s.slice(s.indexOf("<span",index)+"<span style=display:none>".length+2,s.indexOf("</span",index));
    });

    const profesionalName = professional.name.split(" ");
    var s = profesionalName[profesionalName.length-1];
    var i;
    for(i=0 ; i < profesionalName.length-1 ; i++){
        s =s + " " + profesionalName[i];
    }
    console.log(s);
    const lastName = (body[1][0] === professional.lastName);
    const name = (body[1][1].trim().split("</a>")[0] === s);
    const profession = (body[2].trim() === professional.professionalTitle.profession.name);
    const university = (body[3].trim() === professional.professionalTitle.university);
    const ruts = (rut === rutp);
    
    if( lastName && name && profession && university && ruts) return true;
    return false;

}



exports.Professional = Professional;
exports.validate = validateProfessional;
exports.validateEdit = validateEdit;
exports.validateTitle = validateTitle;