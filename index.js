const error = require('./middleware/error');
const config = require('config');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const mongoose = require('mongoose');
//------ routes
const professions = require('./routes/professions');
const attentionTypes = require('./routes/attentionTypes');
const regProfessionals = require('./routes/registerProfessional');
const auth = require('./routes/auth');
//------
const express = require('express');
const app = express();

if(!config.get('jwtPrivateKey')){
  console.error('FATAL ERROR: jwtPrivateKey is not set.');
  process.exit(1);
}

mongoose.connect('mongodb://localhost/bhealth', { useNewUrlParser: true })
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error('Could not connect to MongoDB...'));

app.use(express.json());
app.use('/api/attentionTypes', attentionTypes);
app.use('/api/professions', professions);
app.use('/api/auth', auth);
app.use('/api/registerProfessional', regProfessionals);

//app.use(error);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));