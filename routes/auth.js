const _ = require('lodash');
const {Professional} = require('../models/professional');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const Joi = require('joi');
const bcrypt = require('bcrypt');

router.post('/professional', async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let professional = await Professional.findOne({email:req.body.email});
  if(!professional) return res.status(400).send('Invalid email or password');

  const validPassword = await bcrypt.compare(req.body.password, professional.password);
  if(!validPassword) return res.status(400).send('Invalid email or password');


  const token = professional.generateAuthToken();
  res.send(token);
});


function validate(req) {
    const schema = {
      email: Joi.string().min(3).max(255).required().email(),
      password: Joi.string().min(3).max(1024).required()
    };

    return Joi.validate(req, schema);
}

module.exports = router;