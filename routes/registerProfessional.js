const auth = require('../middleware/auth');
const {Professional,validate,validateTitle} = require('../models/professional');
const {Profession} = require('../models/profession')
const _ = require('lodash')
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');


router.post('/', async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    
    const profession = await Profession.findById(req.body.professionalTitle.idProfession);
    if (!profession) return res.status(400).send('Invalid profession.');

    

    let professional = await Professional.findOne({email:req.body.email}, {rut:req.body.rut});
    if(professional) return res.status(400).send('Professional already registrated');
    
    try{
        professional = new Professional( _.pick(req.body,['rut', 'name',
                    'lastName','email','phone','address',
                    'password','dateOfBirth','evaluation',
                    'personalPhoto','professionalTitle','attentionsAvailable']) );
        const salt = await bcrypt.genSalt(10);

        const newProfression = await new Profession({
            _id: profession.id,
            name: profession.name
        });

        professional.professionalTitle.profession =  newProfression;
    
        professional.password = await bcrypt.hash(professional.password,salt);
        const isValidated = await validateTitle(professional);
        console.log(isValidated);
        professional.isValidated = isValidated;
        await professional.save();
        
    }
    catch(ex){
        res.status(500).send('Something failed. Professional not registred.');
    }

    const token = professional.generateAuthToken();
    res.header('x-auth-token',token).send( _.pick(professional, ['_id',
                'name','email','isValidated']));
});




module.exports = router;