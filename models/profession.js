const Joi = require('joi');
const mongoose = require('mongoose');

const Profession = mongoose.model('Profession', new mongoose.Schema({
    name: {
        type: String,
        minlength: 5,
        maxlength: 50,
        required: true
    },
    attentionTypes:[ {
        _idType: String
        }
        ]
}));


function validateProfession(profession) {
    const schema = {
        name: Joi.string().min(5).max(50).required(),
        attentionType: Joi.array().items(Joi.objectId())
    };
    return Joi.validate(profession, schema);
}

exports.Profession = Profession;
exports.validate = validateProfession;