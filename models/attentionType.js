const Joi = require('joi');
const mongoose = require('mongoose');

const AttentionType = mongoose.model('AttentionType',  new mongoose.Schema({
    name: {
        type: String,
        minlength: 5,
        maxlength: 50,
        required: true
    },
    description: {
        type: String,
        minlength: 5,
        maxlength: 255,
        required: true
    },
    reqPrescription:{
        type: Boolean,
        required: true
    },
    price: {
        type: Number,
        minlength: 1000,
        maxlength: 5000000,
        required: true
    }
 }));

function validateAttentionType( attentionType ){
    const schema = {
        name: Joi.string().min(5).max(50).required(),
        description: Joi.string().min(5).max(255).required(),
        reqPrescription: Joi.boolean().required(),
        price: Joi.number().min(10000).max(5000000).required()
    };
    return Joi.validate(attentionType, schema);
};

exports.AttentionType = AttentionType;
exports.validate = validateAttentionType;