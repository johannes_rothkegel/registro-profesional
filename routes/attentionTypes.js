const {AttentionType,  validate}  = require('../models/attentionType');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

router.post('/', async (req, res) => {

    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const attentionType = new AttentionType({
      name: req.body.name,
      description: req.body.description,
      reqPrescription: req.body.reqPrescription,
      price: req.body.price
    });

    await attentionType.save();

    res.send(attentionType);
 });

router.get('/', async (req,res) => {
  const attentionType = await AttentionType.find().sort('name');
  res.send(attentionType);
});

router.get('/:id', async (req,res) => {
  const attentionType = await AttentionType.findById(req.params.id);
  if (!attentionType) return res.status(400).send('Invalid id for profession.');
  res.send(attentionType);
});

module.exports = router;